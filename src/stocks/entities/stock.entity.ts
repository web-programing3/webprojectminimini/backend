import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @Column()
  minimum: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column()
  price: number;
}
