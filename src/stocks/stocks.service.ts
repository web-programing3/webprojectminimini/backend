import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';
import { StocksModule } from './stocks.module';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  // lastid: number = 14;
  // stocks: Stock[] = [
  //   { id: 1, name: 'ไซรัป', minimum: 10, balance: 15, unit: 'ขวด', price: 225 },
  //   {
  //     id: 2,
  //     name: 'เมล็ดกาแฟ',
  //     minimum: 10,
  //     balance: 4,
  //     unit: 'กิโลกรัม',
  //     price: 400,
  //   },
  //   {
  //     id: 3,
  //     name: 'ผงชาไทย',
  //     minimum: 10,
  //     balance: 6,
  //     unit: 'กิโลกรัม',
  //     price: 300,
  //   },
  //   {
  //     id: 4,
  //     name: 'ผงโกโก้',
  //     minimum: 10,
  //     balance: 5,
  //     unit: 'กิโลกรัม',
  //     price: 350,
  //   },
  //   {
  //     id: 5,
  //     name: 'ผงชาเขียว',
  //     minimum: 10,
  //     balance: 8,
  //     unit: 'กิโลกรัม',
  //     price: 360,
  //   },
  //   {
  //     id: 6,
  //     name: 'นมจืด',
  //     minimum: 10,
  //     balance: 3,
  //     unit: 'กิโลกรัม',
  //     price: 120,
  //   },
  //   {
  //     id: 7,
  //     name: 'น้ำเชื่อม',
  //     minimum: 10,
  //     balance: 8,
  //     unit: 'ลิตร',
  //     price: 250,
  //   },
  //   {
  //     id: 8,
  //     name: 'วิปปิ่งครีม',
  //     minimum: 10,
  //     balance: 11,
  //     unit: 'ลิตร',
  //     price: 150,
  //   },
  //   {
  //     id: 9,
  //     name: 'น้ำผึ้ง',
  //     minimum: 10,
  //     balance: 14,
  //     unit: 'ลิตร',
  //     price: 95,
  //   },
  //   {
  //     id: 10,
  //     name: 'ครีมเทียม',
  //     minimum: 10,
  //     balance: 12,
  //     unit: 'ลิตร',
  //     price: 120,
  //   },
  //   {
  //     id: 11,
  //     name: 'น้ำตาลคาราเมล',
  //     minimum: 10,
  //     balance: 4,
  //     unit: 'กิโลกรัม',
  //     price: 120,
  //   },
  //   {
  //     id: 12,
  //     name: 'ไข่มุก',
  //     minimum: 10,
  //     balance: 6,
  //     unit: 'กิโลกรัม',
  //     price: 35,
  //   },
  //   {
  //     id: 13,
  //     name: 'เมล็ดมะขามคั่ว',
  //     minimum: 10,
  //     balance: 10,
  //     unit: 'กิโลกรัม',
  //     price: 250,
  //   },
  //   {
  //     id: 14,
  //     name: 'กีวี',
  //     minimum: 10,
  //     balance: 11,
  //     unit: 'กิโลกรัม',
  //     price: 150,
  //   },
  // ];
  constructor(
    @InjectRepository(Stock)
    private stocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto): Promise<Stock> {
    return this.stocksRepository.save(createStockDto);
  }

  findAll(): Promise<Stock[]> {
    return this.stocksRepository.find();
  }

  findOne(id: number) {
    return this.stocksRepository.findOneBy({ id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    await this.stocksRepository.update(id, updateStockDto);
    const stock = await this.stocksRepository.findOneBy({ id });
    return stock;
  }

  async remove(id: number) {
    const deleteStock = await this.stocksRepository.findOneBy({ id });
    return this.stocksRepository.remove(deleteStock);
  }
}
